<?php

namespace Drupal\commerce_civicrm_event_registration;

use Drupal\civicrm_entity\CiviCrmApiInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Service class containing various CiviCRM utility functions.
 */
class CommerceCivicrmRegistrationHelper {

  /**
   * Drupal\civicrm_entity\CiviCrmApiInterface definition.
   *
   * @var \Drupal\civicrm_entity\CiviCrmApiInterface
   */
  protected $civicrmEntityApi;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * CommerceCivicrmRegistrationHelper constructor.
   *
   * @param \Drupal\civicrm_entity\CiviCrmApiInterface $civicrm_entity_api
   *   CiviCRM Entity API service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user service.
   */
  public function __construct(CiviCrmApiInterface $civicrm_entity_api, EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user) {
    $this->civicrmEntityApi = $civicrm_entity_api;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Get contact id of logged in user.
   */
  public function getLoggedInUserContactId() {
    $uid = $this->currentUser->id();
    if (empty($uid)) {
      return FALSE;
    }
    $contact_id = $this->getContactIdForUser($uid);
    return $contact_id;
  }

  /**
   * Get contact id for user, given uid.
   *
   * @param int $uid
   *   The user uid.
   */
  public function getContactIdForUser($uid) {
    if (!empty($uid)) {
      $result = $this->civicrmEntityApi->get('uf_match', [
        'sequential' => TRUE,
        'uf_id' => $uid,
      ]);
      if (!empty($result[0]['contact_id'])) {
        return $result[0]['contact_id'];
      }
    }
    return FALSE;
  }

  /**
   * Search for a contact.
   *
   * @param array $params
   *   Params to use to find contact record.
   *
   * @return int|bool
   *   Contact id or FALSE.
   */
  public function findContactFromParams(array $params) {
    $result = $this->civicrmEntityApi->get('contact', ['sequential' => TRUE] + $params);
    if (!empty($result[0]['contact_id'])) {
      return $result[0]['contact_id'];
    }
    return FALSE;
  }

  /**
   * Find participant record given a contact id and event id.
   *
   * @param int $contact_id
   *   The contact id.
   * @param int $event_id
   *   The event id.
   *
   * @return int|bool
   *   Participant or FALSE.
   */
  public function findParticipantRecordForContactForEvent($contact_id, $event_id) {
    if (!empty($contact_id) && !empty($event_id)) {
      $result = $this->civicrmEntityApi->get('participant', ['sequential' => TRUE] + [
        'contact_id' => $contact_id,
        'event_id' => $event_id,
      ]);
      if (!empty($result[0]['id'])) {
        return $result[0]['id'];
      }
    }
    return FALSE;
  }

  /**
   * Find participant record given a contact id and event id, and a status.
   *
   * @param int $contact_id
   *   The contact id.
   * @param int $event_id
   *   The event id.
   * @param int|string $status_id
   *   The status id.
   *
   * @return int|bool
   *   Participant or FALSE.
   */
  public function findParticipantRecordForContactForEventWithStatus($contact_id, $event_id, $status_id) {
    if (!empty($contact_id) && !empty($event_id)) {
      $result = $this->civicrmEntityApi->get('participant', ['sequential' => TRUE] + [
        'contact_id' => $contact_id,
        'event_id' => $event_id,
        'status_id' => $status_id,
      ]);
      if (!empty($result[0]['id'])) {
        return $result[0]['id'];
      }
    }
    return FALSE;
  }

  /**
   * Create participant record.
   *
   * @param int $contact_id
   *   The contact id.
   * @param int $event_id
   *   The event id.
   * @param float $fee_amount
   *   The adjusted fee amount.
   * @param int|null $registered_by_id
   *   Participant id of the primary registrant.
   * @param int|string $status_id
   *   The status to create participant record with.
   *
   * @return mixed|void
   *   Participant id.
   */
  public function createParticipantRecord($contact_id, $event_id, $fee_amount, $registered_by_id = NULL, $status_id = "Pending from incomplete transaction") {
    if (!empty($contact_id) && !empty($event_id)) {
      $params = [
        "sequential" => TRUE,
        "event_id" => $event_id,
        "contact_id" => $contact_id,
        "status_id" => $status_id,
        "role_id" => "Attendee",
        "register_date" => date('Y-m-d H:i:s'),
        "fee_amount" => $fee_amount,
      ];
      if (!empty($registered_by_id)) {
        $params["registered_by_id"] = $registered_by_id;
      }
      $result = $this->civicrmEntityApi->save('participant', $params);
      return $result['id'];
    }
  }

  /**
   * Get contact info given participant id.
   *
   * @param int $participant_id
   *   Participant id.
   *
   * @return array
   *   Contact values.
   */
  public function getContactDetailsFromParticipantId($participant_id) {
    $return_values = [];
    $participant_result = $this->civicrmEntityApi->get('participant', [
      'sequential' => TRUE,
      'id' => $participant_id,
    ]);
    if (!empty($participant_result[0]['contact_id'])) {
      $contact_result = $this->civicrmEntityApi->get('contact', [
        'sequential' => TRUE,
        'id' => $participant_result[0]['contact_id'],
      ]);
      if (!empty($contact_result[0]['id'])) {
        $return_values['contact_id'] = $contact_result[0]['id'];
        $return_values['first_name'] = $contact_result[0]['first_name'];
        $return_values['last_name'] = $contact_result[0]['last_name'];
        $return_values['email'] = $contact_result[0]['email'];
        $logged_in_user_contact_id = $this->getLoggedInUserContactId();
        if (!empty($logged_in_user_contact_id) && $return_values['contact_id'] == $logged_in_user_contact_id) {
          $return_values['logged_in_user'] = TRUE;
        }
      }
    }
    return $return_values;
  }

  /**
   * Create contribution data for an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param bool $in_cart
   *   Is the order still in cart processing.
   * @param int $contact_id
   *   Contact ID.
   */
  public function createContributionForOrder(OrderInterface $order, $in_cart = FALSE, $contact_id = 0) {
    $total = $order->getTotalPrice()->getNumber();
    $paid = $order->isPaid();

    if (empty($contact_id)) {
      $customer = $order->getCustomer();
      $uid = $customer->id();
      $contact_id = $this->getContactIdForUser($uid);
    }

    $params = [
      'sequential' => FALSE,
      'contact_id' => $contact_id,
      "financial_type_id" => "Event Fee",
      "receive_date" => date('Y-m-d H:i:s'),
      "total_amount" => round($total, 2),
      'source' => "Drupal commerce order id: " . $order->id(),
      'contribution_status_id' => "Pending",
      'is_pay_later' => 1,
    ];
    if ($in_cart) {
      // Tried to use "In Progress" .. but API restricts changing status from
      // "In Progress" to "Pending".
      $params['contribution_status_id'] = "Pending";
    }
    if ($paid) {
      $params['contribution_status_id'] = 'Completed';
      $params['is_pay_later'] = 0;
    }
    $contribution_result = $this->civicrmEntityApi->save('contribution', $params);
    return $contribution_result;
  }

  /**
   * Update contribution status to completed.
   *
   * @param int $contribution_id
   *   The contribution id.
   */
  public function updateContributionCompleted($contribution_id) {
    if (!empty($contribution_id)) {
      $params = [
        'contribution_status_id' => "Completed",
        'is_pay_later' => 0,
        'id' => $contribution_id,
        "financial_type_id" => "Event Fee",
      ];
      $this->civicrmEntityApi->save('contribution', $params);
    }
  }

  /**
   * Update contribution status to pending.
   *
   * @param int $contribution_id
   *   The contribution id.
   */
  public function updateContributionPending($contribution_id) {
    if (!empty($contribution_id)) {
      $params = [
        'contribution_status_id' => "Pending",
        "financial_type_id" => "Event Fee",
        'is_pay_later' => 1,
        'id' => $contribution_id,
      ];
      $this->civicrmEntityApi->save('contribution', $params);
    }
  }

  /**
   * Get contribution data for an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   */
  public function getContributionForOrder(OrderInterface $order) {
    $contribution_field_data = $order->get('field_civicrm_contribution')->getValue();
    if (!empty($contribution_field_data[0]['target_id'])) {
      $contribution_id = $contribution_field_data[0]['target_id'];
      $contribution_result = $this->civicrmEntityApi->get('contribution', [
        'id' => $contribution_id,
      ]);
      return !empty($contribution_result[$contribution_id]) ? $contribution_result[$contribution_id] : FALSE;
    }
    return FALSE;
  }

  /**
   * Manage Civi records for the order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param bool $in_cart
   *   Is the order still in cart?
   * @param int $contact_id
   *   Contact ID.
   */
  public function manageCiviRecordsForOrder(OrderInterface $order, $in_cart = FALSE, $contact_id = 0) {
    if ($contribution = $this->getContributionForOrder($order)) {
      // Contribution already exists.
      if ($order->isPaid()) {
        $this->updateContributionCompleted($contribution['id']);
      }
      elseif (!$in_cart) {
        $this->updateContributionPending($contribution['id']);
      }
    }
    else {
      // Contribution doesn't exist, create it.
      $contribution = $this->createContributionForOrder($order, $in_cart, $contact_id);
      // Reference the contribution from the order.
      if ($order->hasField('field_civicrm_contribution')) {
        $order->field_civicrm_contribution[0] = ['target_id' => $contribution['id']];
        $order->save();
      }
    }
    // Create ParticipantPayment records as necessary.
    if (!empty($contribution['id'])) {
      foreach ($order->getItems() as $order_item) {
        $product_variation = $order_item->getPurchasedEntity();
        $productType = $product_variation->get('type')[0]->get('target_id')->getValue();
        if ($productType == 'civicrm_event') {
          $stored_participant_ids_field_data = $order_item->get('field_civicrm_participants')->getValue();
          foreach ($stored_participant_ids_field_data as $q_data) {
            $participant_id = $q_data['target_id'];
            // If order is paid, update participant record status.
            if ($order->isPaid()) {
              $this->civicrmEntityApi->save('participant', [
                'id' => $participant_id,
                'participant_status' => 'Registered',
                'participant_status_id' => 1,
              ]);
            }
            else {
              $participant_update_params = [
                'id' => $participant_id,
              ];
              if ($in_cart) {
                $participant_update_params['participant_status'] = "Pending from incomplete transaction";
                $participant_update_params['participant_status_id'] = 6;
              }
              else {
                $participant_update_params['participant_status'] = "Pending from pay later";
                $participant_update_params['participant_status_id'] = 5;
              }
              $this->civicrmEntityApi->save('participant', $participant_update_params);
            }
            // Does ParticipantPayment record exist?
            $participant_payment_result = $this->civicrmEntityApi->get('participant_payment', [
              'sequential' => TRUE,
              'contribution_id' => $contribution['id'],
              'participant_id' => $participant_id,
            ]);
            // If not, create.
            if (empty($participant_payment_result[0]['id'])) {
              $this->civicrmEntityApi->save('participant_payment', [
                'contribution_id' => $contribution['id'],
                'participant_id' => $participant_id,
              ]);
            }
          }
        }
      }
    }
  }

}
