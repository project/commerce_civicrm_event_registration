<?php

namespace Drupal\commerce_civicrm_event_registration\Plugin\Commerce\CheckoutPane;

use Drupal\civicrm_entity\CiviCrmApiInterface;
use Drupal\commerce\InlineFormManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_civicrm_event_registration\CommerceCivicrmRegistrationHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows customers to add comments to the order.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_civicrm_participant_registration_pane",
 *   label = @Translation("Registration Information"),
 *   default_step = "registration_information",
 *   wrapper_element = "fieldset",
 * )
 */
class CommerceCivicrmParticipantRegistrationPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected $inlineFormManager;

  /**
   * Drupal\civicrm_entity\CiviCrmApiInterface definition.
   *
   * @var \Drupal\civicrm_entity\CiviCrmApiInterface
   */
  protected $civicrmEntityApi;

  /**
   * Drupal\commerce_civicrm_event_registration.
   *
   * \CommerceCivicrmRegistrationHelper definition.
   *
   * @var \Drupal\commerce_civicrm_event_registration\CommerceCivicrmRegistrationHelper
   */
  protected $civiHelper;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new CommerceCivicrmParticipantRegistrationPane object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Drupal\civicrm_entity\CiviCrmApiInterface $civicrm_entity_api
   *   The CiviCRM Entity API service.
   * @param \Drupal\commerce_civicrm_event_registration\CommerceCivicrmRegistrationHelper $civicrm_helper
   *   The CiviCRM helper.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, InlineFormManager $inline_form_manager, CiviCrmApiInterface $civicrm_entity_api, CommerceCivicrmRegistrationHelper $civicrm_helper, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->inlineFormManager = $inline_form_manager;
    $this->civicrmEntityApi = $civicrm_entity_api;
    $this->civiHelper = $civicrm_helper;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('civicrm_entity.api'),
      $container->get('commerce_civicrm_event_registration.civicrm_helper'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    // Only show pane if an order item of product type civicrm_event is in
    // the order.
    foreach ($this->order->getItems() as $order_item) {
      $product_variation = $order_item->getPurchasedEntity();
      $productType = $order_item->getPurchasedEntity()->get('type')[0]->get('target_id')->getValue();
      if ($productType == 'civicrm_event') {
        $event_id = _commerce_civicrm_event_registration_product_get_event_id($product_variation);
        if (!empty($event_id)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary() {
    $summary = [];
    $events = [];
    foreach ($this->order->getItems() as $key => $order_item) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
      $product_variation = $order_item->getPurchasedEntity();
      $productType = $product_variation->get('type')[0]->get('target_id')->getValue();
      if ($productType == 'civicrm_event') {
        $events[$key]['title'] = $product_variation->getTitle();
        $stored_participant_ids_field_data = $order_item->get('field_civicrm_participants')->getValue();
        if (!empty($stored_participant_ids_field_data)) {
          foreach ($stored_participant_ids_field_data as $q => $q_data) {
            $participant_id = $q_data['target_id'];
            $stored_participant_data = $this->civiHelper->getContactDetailsFromParticipantId($participant_id);
            $events[$key]['participants'][$q]['first_name'] = !empty($stored_participant_data['first_name']) ? $stored_participant_data['first_name'] : '';
            $events[$key]['participants'][$q]['last_name'] = $stored_participant_data['last_name'] ? $stored_participant_data['last_name'] : '';
            $events[$key]['participants'][$q]['email'] = $stored_participant_data['email'] ? $stored_participant_data['email'] : '';
          }
        }
      }
    }
    $summary = [
      '#theme' => 'commerce_civicrm_event_registration_registration_summary',
      '#events' => $events,
    ];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    foreach ($this->order->getItems() as $key => $order_item) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
      $product_variation = $order_item->getPurchasedEntity();
      $productType = $product_variation->get('type')[0]->get('target_id')->getValue();
      if ($productType == 'civicrm_event') {
        $pane_form['registration_information'][$key]['event'] = [
          '#type' => 'details',
          '#title' => $product_variation->getTitle(),
          '#open' => TRUE,
        ];
        $quantity = $order_item->getQuantity();
        // Get event id.
        $event_id = _commerce_civicrm_event_registration_product_get_event_id($product_variation);
        $pane_form['registration_information'][$key]['event_id'] = [
          '#type' => 'hidden',
          '#value' => $event_id,
        ];
        $pane_form['registration_information'][$key]['quantity'] = [
          '#type' => 'hidden',
          '#value' => $quantity,
        ];

        // Do the line items have referenced participant records?
        $stored_participant_ids_field_data = $order_item->get('field_civicrm_participants')->getValue();
        $stored_participant_ids = [];
        foreach ($stored_participant_ids_field_data as $q => $q_data) {
          $stored_participant_ids[$q] = $q_data['target_id'];
        }

        // Is logged in user anonymous?
        $logged_in_anonymous = $this->currentUser->isAnonymous();

        // If so fetch contact data for form state default values.
        for ($i = 0; $i < $quantity; $i++) {
          if (!empty($stored_participant_ids[$i])) {
            $stored_participant_data = $this->civiHelper->getContactDetailsFromParticipantId($stored_participant_ids[$i]);
          }
          else {
            $stored_participant_data = [];
          }

          $pane_form['registration_information'][$key]['event'][$i]['participant'] = [
            '#type' => 'details',
            '#title' => 'Participant ' . ($i + 1),
            '#open' => TRUE,
          ];
          if ($i == 0) {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['logged_in_user'] = [
              '#type' => 'checkbox',
              '#title' => 'Register myself',
              '#description' => $this->t('Uncheck this to register someone else'),
              '#default_value' => 1,
              '#return_value' => 1,
            ];
            if (!empty($stored_participant_data['logged_in_user']) || (empty($stored_participant_ids[$i]))) {
              $pane_form['registration_information'][$key]['event'][$i]['participant']['logged_in_user']['#default_value'] = 1;
            }
            else {
              $pane_form['registration_information'][$key]['event'][$i]['participant']['logged_in_user']['#default_value'] = 0;
            }

            // If anonymous user force registration to be for "yourself".
            // So contact_id and Drupal user can be easily matched.
            if ($logged_in_anonymous) {
              $pane_form['registration_information'][$key]['event'][$i]['participant']['logged_in_user']['#default_value'] = 1;
              $pane_form['registration_information'][$key]['event'][$i]['participant']['logged_in_user']['#disabled'] = TRUE;
            }
          }
          // First name.
          $pane_form['registration_information'][$key]['event'][$i]['participant']['first_name'] = [
            '#type' => 'textfield',
            '#title' => 'First Name',
          ];
          if ($i == 0) {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['first_name']['#states'] = [
              'visible' => [
                ':input[name="commerce_civicrm_participant_registration_pane[registration_information][' . $key . '][event][' . $i . '][participant][logged_in_user]"]' => ['checked' => FALSE],
              ],
              'required' => [
                ':input[name="commerce_civicrm_participant_registration_pane[registration_information][' . $key . '][event][' . $i . '][participant][logged_in_user]"]' => ['checked' => FALSE],
              ],
            ];

            if ($logged_in_anonymous) {
              unset($pane_form['registration_information'][$key]['event'][$i]['participant']['first_name']['#states']);
              $pane_form['registration_information'][$key]['event'][$i]['participant']['first_name']['#required'] = TRUE;
            }
          }
          else {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['first_name']['#required'] = TRUE;
          }
          if (!empty($stored_participant_data['first_name'])) {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['first_name']['#default_value'] = $stored_participant_data['first_name'];
          }
          // Last name.
          $pane_form['registration_information'][$key]['event'][$i]['participant']['last_name'] = [
            '#type' => 'textfield',
            '#title' => 'Last Name',
          ];
          if ($i == 0) {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['last_name']['#states'] = [
              'visible' => [
                ':input[name="commerce_civicrm_participant_registration_pane[registration_information][' . $key . '][event][' . $i . '][participant][logged_in_user]"]' => ['checked' => FALSE],
              ],
              'required' => [
                ':input[name="commerce_civicrm_participant_registration_pane[registration_information][' . $key . '][event][' . $i . '][participant][logged_in_user]"]' => ['checked' => FALSE],
              ],
            ];

            if ($logged_in_anonymous) {
              unset($pane_form['registration_information'][$key]['event'][$i]['participant']['last_name']['#states']);
              $pane_form['registration_information'][$key]['event'][$i]['participant']['last_name']['#required'] = TRUE;
            }
          }
          else {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['last_name']['#required'] = TRUE;
          }

          if (!empty($stored_participant_data['last_name'])) {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['last_name']['#default_value'] = $stored_participant_data['last_name'];
          }

          // Email.
          $pane_form['registration_information'][$key]['event'][$i]['participant']['email'] = [
            '#type' => 'email',
            '#title' => 'Email',
          ];
          if ($i == 0) {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['email']['#states'] = [
              'visible' => [
                ':input[name="commerce_civicrm_participant_registration_pane[registration_information][' . $key . '][event][' . $i . '][participant][logged_in_user]"]' => ['checked' => FALSE],
              ],
              'required' => [
                ':input[name="commerce_civicrm_participant_registration_pane[registration_information][' . $key . '][event][' . $i . '][participant][logged_in_user]"]' => ['checked' => FALSE],
              ],
            ];
            if ($logged_in_anonymous) {
              unset($pane_form['registration_information'][$key]['event'][$i]['participant']['email']['#states']);
              $pane_form['registration_information'][$key]['event'][$i]['participant']['email']['#required'] = TRUE;
            }
          }
          else {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['email']['#required'] = TRUE;
          }
          if (!empty($stored_participant_data['email'])) {
            $pane_form['registration_information'][$key]['event'][$i]['participant']['email']['#default_value'] = $stored_participant_data['email'];
          }
        }
      }
    }
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    $current_order_items = [];
    $registered_by_participant_id = NULL;
    $first_contact_id = 0;
    foreach ($this->order->getItems() as $order_item) {
      $current_order_items[] = $order_item;
    }
    // Foreach event.
    foreach ($values['registration_information'] as $key => $value) {
      $event_id = $value['event_id'];
      $order_item = $current_order_items[$key];
      $stored_participant_ids = $order_item->get('field_civicrm_participants')->getValue();
      $adjusted_price = $order_item->getAdjustedUnitPrice()->getNumber();

      // Foreach participant.
      foreach ($value['event'] as $k => $p) {
        $part_input = $p['participant'];

        if (!empty($part_input['logged_in_user']) && $this->currentUser->isAuthenticated()) {
          $contact_id = $this->civiHelper->getLoggedInUserContactId();
        }
        else {
          $contact_id = $this->civiHelper->findContactFromParams([
            'first_name' => $part_input['first_name'],
            'last_name' => $part_input['last_name'],
            'email' => $part_input['email'],
          ]);
          if (empty($contact_id)) {
            // Create a contact.
            $contact_create_result = $this->civicrmEntityApi->save('Contact', [
              'sequential' => TRUE,
              'contact_type' => 'Individual',
              'first_name' => $part_input['first_name'],
              'last_name' => $part_input['last_name'],
              'email' => $part_input['email'],
            ]);
            if (!empty($contact_create_result['id'])) {
              $contact_id = $contact_create_result['id'];
            }
            else {
              // Something went wrong.
            }
          }
        }
        if (empty($first_contact_id)) {
          $first_contact_id = $contact_id;
        }
        // Is there already a referenced participant record?
        $existing_participant_id = $this->civiHelper->findParticipantRecordForContactForEvent($contact_id, $event_id);

        // Create participant record?
        if (empty($existing_participant_id)) {
          if ($k == 0) {
            $participant_id = $registered_by_participant_id = $this->civiHelper->createParticipantRecord($contact_id, $event_id, $adjusted_price);
          }
          else {
            $participant_id = $this->civiHelper->createParticipantRecord($contact_id, $event_id, $adjusted_price, $registered_by_participant_id);
          }
        }
        else {
          // Participant record existed for contact id.
          $participant_id = $existing_participant_id;
          if ($k == 0) {
            $registered_by_participant_id = $participant_id;
          }
          // Update fee amount in case existing value doesn't reflect
          // adjusted price.
          $this->civicrmEntityApi->save('participant', [
            'id' => $participant_id,
            'fee_amount' => $adjusted_price,
          ]);
        }

        if (!empty($stored_participant_ids[$k]['target_id']) && $stored_participant_ids[$k]['target_id'] != $participant_id) {
          // Participant has changed, delete the existing participant record.
          try {
            $this->civicrmEntityApi->delete('participant', ['id' => $stored_participant_ids[$k]['target_id']]);
          }
          catch (\Exception $e) {

          }
        }
        // Update the referenced target_id if necessary.
        $order_item->field_civicrm_participants[$k] = ['target_id' => $participant_id];

      } // End for each participant.
      // Check for too many participant records.
      // Would happen if user went back to cart and updated quantity less.
      if (count($stored_participant_ids) > count($value['event'])) {
        $stored_participants_count = count($stored_participant_ids);
        $quantity_participants_count = count($value['event']);
        for ($z = $quantity_participants_count; $z < $stored_participants_count; $z++) {
          try {
            $this->civicrmEntityApi->delete('participant', ['id' => $stored_participant_ids[$z]['target_id']]);
          }
          catch (\Exception $e) {

          }
          unset($order_item->field_civicrm_participants[$z]);
        }
      }
      // Save the order item.
      $order_item->save();
    } // End for each event.
    // Create contribution and participant payment records. Passing contact_id
    // here only makes sense if, Anonymous user, is registering themselves. If
    // not .. go back later and update contribution record?
    $logged_in_contact_id = $this->civiHelper->getLoggedInUserContactId();
    if (empty($logged_in_contact_id)) {
      if (!empty($first_contact_id)) {
        $contribution_contact_id = $first_contact_id;
      }
      else {
        $contribution_contact_id = $contact_id;
      }
    }
    else {
      $contribution_contact_id = $logged_in_contact_id;
    }

    $contribution_contact_id = $contribution_contact_id ?? 0;
    $this->civiHelper->manageCiviRecordsForOrder($this->order, TRUE, $contribution_contact_id);
  }

}
