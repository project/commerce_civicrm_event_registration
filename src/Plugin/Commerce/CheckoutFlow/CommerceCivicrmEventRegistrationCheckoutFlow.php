<?php

namespace Drupal\commerce_civicrm_event_registration\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;

/**
 * Provides the CiviCRM Event registration multistep checkout flow.
 *
 * @CommerceCheckoutFlow(
 *   id = "multistep_commerce_civicrm_event_registration",
 *   label = "CiviCRM Event Registration Checkout Flow",
 * )
 */
class CommerceCivicrmEventRegistrationCheckoutFlow extends CheckoutFlowWithPanesBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    // Note that previous_label and next_label are not the labels
    // shown on the step itself. Instead, they are the labels shown
    // when going back to the step, or proceeding to the step.
    return [
      'login' => [
        'label' => $this->t('Login'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => FALSE,
      ],
      'registration_information' => [
        'label' => $this->t('Registration Information'),
        'previous_label' => $this->t('Go back'),
        'next_label' => $this->t('Continue'),
        'has_sidebar' => FALSE,
      ],
      'order_information' => [
        'label' => $this->t('Order information'),
        'has_sidebar' => TRUE,
        'previous_label' => $this->t('Go back'),
        'next_label' => $this->t('Continue'),
      ],
      'review' => [
        'label' => $this->t('Review'),
        'next_label' => $this->t('Continue to review'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => TRUE,
      ],
    ] + parent::getSteps();
  }

}
