<?php

namespace Drupal\commerce_civicrm_event_registration\EventSubscriber;

use Drupal\civicrm_entity\CiviCrmApiInterface;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_civicrm_event_registration\CommerceCivicrmRegistrationHelper;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class OrderEventSubscriber, respond to commerce order events.
 */
class OrderEventSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\civicrm_entity\CiviCrmApiInterface definition.
   *
   * @var \Drupal\civicrm_entity\CiviCrmApiInterface
   */
  protected $civicrmEntityApi;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface Definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\commerce_civicrm_event_registration.
   *
   * \CommerceCivicrmRegistrationHelper definition.
   *
   * @var \Drupal\commerce_civicrm_event_registration\CommerceCivicrmRegistrationHelper
   */
  protected $civiHelper;

  /**
   * Constructs a new OrderEventSubscriber object.
   *
   * @param \Drupal\civicrm_entity\CiviCrmApiInterface $civicrm_entity_api
   *   CiviCRM Entity API service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_civicrm_event_registration\CommerceCivicrmRegistrationHelper $civicrm_helper
   *   The CiviCRM helper.
   */
  public function __construct(CiviCrmApiInterface $civicrm_entity_api, EntityTypeManagerInterface $entity_type_manager, CommerceCivicrmRegistrationHelper $civicrm_helper) {
    $this->civicrmEntityApi = $civicrm_entity_api;
    $this->entityTypeManager = $entity_type_manager;
    $this->civiHelper = $civicrm_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      OrderEvents::ORDER_PAID => ['onOrderPaid', -50],
      CheckoutEvents::COMPLETION => ['onOrderCompletion'],
    ];
    return $events;
  }

  /**
   * Respond to order paid event.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order has been paid event.
   */
  public function onOrderPaid(OrderEvent $event) {
    $order = $event->getOrder();
    $this->civiHelper->manageCiviRecordsForOrder($order, FALSE);
  }

  /**
   * Respond to order completion event.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order event.
   */
  public function onOrderCompletion(OrderEvent $event) {
    $order = $event->getOrder();
    $this->civiHelper->manageCiviRecordsForOrder($order, FALSE);
  }

}
