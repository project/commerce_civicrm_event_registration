<?php

namespace Drupal\commerce_civicrm_event_registration\EventSubscriber;

use Drupal\civicrm_entity\CiviCrmApiInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\commerce_cart\Event\CartOrderItemRemoveEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CartEventSubscriber. Respond to commerce cart events.
 */
class CartEventSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\civicrm_entity\CiviCrmApiInterface definition.
   *
   * @var \Drupal\civicrm_entity\CiviCrmApiInterface
   */
  protected $civicrmEntityApi;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CartEventSubscriber object.
   *
   * @param \Drupal\civicrm_entity\CiviCrmApiInterface $civicrm_entity_api
   *   The entity type manager.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(CiviCrmApiInterface $civicrm_entity_api, CartManagerInterface $cart_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->civicrmEntityApi = $civicrm_entity_api;
    $this->cartManager = $cart_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      CartEvents::CART_ORDER_ITEM_REMOVE => ['onCartOrderItemRemove', -50],
    ];
    return $events;
  }

  /**
   * Remove participant records referenced by an order item.
   *
   * @param \Drupal\commerce_cart\Event\CartOrderItemRemoveEvent $event
   *   The cart event.
   */
  public function onCartOrderItemRemove(CartOrderItemRemoveEvent $event) {
    $order_item = $event->getOrderItem();
    if ($order_item->hasField('field_civicrm_participants')) {
      $product_variation = $order_item->getPurchasedEntity();
      $productType = $product_variation->get('type')[0]->get('target_id')->getValue();
      if ($productType == 'civicrm_event') {
        // Remove any referenced participants.
        $referenced_participants = $order_item->get('field_civicrm_participants')->getValue();
        if (!empty($referenced_participants)) {
          foreach ($referenced_participants as $participant) {
            try {
              if (!empty($participant['target_id'])) {
                $this->civicrmEntityApi->delete('participant', ['id' => $participant['target_id']]);
              }
            }
            catch (\Exception $e) {

            }
          }
        }
      }
    }
  }

}
